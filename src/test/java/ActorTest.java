import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.openqa.selenium.JavascriptExecutor;

public class ActorTest {
    public WebDriver driver;
    public String testURL = "http://www.imdb.com";

    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
    }

    @Test
    public void ActorTest() throws InterruptedException {
        driver.manage().window().maximize();
        Thread.sleep(3000);

        WebElement searchTextBox = driver.findElement(By.name("q"));
        searchTextBox.sendKeys("Pedro Pascal");
        searchTextBox.submit();
        Thread.sleep(2000);

        WebElement firstSearch = driver.findElement(By.xpath("/html/body/div[2]/main/div[2]/div[3]/section/div/div[1]/section[2]/div[2]/ul/li[1]/div[2]/div/a"));
        Assert.assertEquals(firstSearch.getText(),"Pedro Pascal");
        firstSearch.click();

        WebElement photos = driver.findElement(By.xpath("/html/body/div[2]/main/div/section[1]/div/section/div/div[1]/section[3]/div[1]/div/a"));
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", photos);
        Thread.sleep(2500);

        WebElement nextButton = driver.findElement(By.xpath("/html/body/div[2]/main/div[2]/div[3]/div[3]"));
        nextButton.click();
        Thread.sleep(1500);
        nextButton.click();
        Thread.sleep(1500);
        nextButton.click();
        Thread.sleep(1500);

        WebElement closeButton = driver.findElement(By.xpath("/html/body/div[2]/main/div[2]/div[2]/div/div[1]/a"));
        closeButton.click();
        Thread.sleep(1500);

        WebElement biographyButton = driver.findElement(By.xpath("/html/body/div[2]/main/div/section[1]/section/div[3]/section/section/div[1]/div/div[2]/ul/li[1]/a"));
        biographyButton.click();

        Thread.sleep(3000);
    }

    @AfterMethod
    public void teardownTest() {
        driver.quit();
    }
}
