import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class WatchlistTest {
    public WebDriver driver;
    public String testURL = "http://www.imdb.com";

    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
    }
    @Test
    public void WatchlistTest() throws InterruptedException {

        driver.manage().window().maximize();
        Thread.sleep(3000);
        WebElement signInButton= driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/div[5]/a/span"));
        signInButton.click();

        WebElement ImdbLoginButton = driver.findElement(By.xpath("/html/body/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[1]/a[1]"));
        ImdbLoginButton.click();

        WebElement usernameTextBox = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[2]/div/form/div/div/div/div[1]/input"));
        usernameTextBox.sendKeys("jokes1809@gmail.com");

        WebElement passwordTextBox = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[2]/div/form/div/div/div/div[2]/input"));
        passwordTextBox.sendKeys("TestingProject1");

        WebElement submitButton = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[2]/div/form/div/div/div/div[3]/span/span/input"));
        submitButton.click();
        Thread.sleep(2000);

        WebElement watchlistButton = driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/div[4]/a/span"));
        watchlistButton.click();
        Thread.sleep(2000);


        WebElement editButton = driver.findElement(By.linkText("EDIT"));
        editButton.click();
        Thread.sleep(2000);

        for (int i=1; i<5; i++) {
            WebElement searchTextBox = driver.findElement(By.id("add-to-list-search"));
            Thread.sleep(1500);
            String movie;
            if (i!=1) {
                movie = "Rush Hour " + String.valueOf(i);
            }
            else{
                movie ="Rush Hour";
            }
            searchTextBox.sendKeys(movie);
            Thread.sleep(1500);
            WebElement addButton = driver.findElement(By.cssSelector("#add-to-list-search-results > a:nth-child(1)"));
            addButton.click();
            Thread.sleep(1500);
        }
        Thread.sleep(3000);
        WebElement deleteCheck = driver.findElement(By.id("totalCheck"));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", deleteCheck);
        Thread.sleep(2000);
        WebElement deleteButton = driver.findElement(By.linkText("DELETE"));
        deleteButton.click();
        Thread.sleep(1000);
        WebElement confirmButton = driver.findElement(By.cssSelector("#delete_items_form > div > input"));
        confirmButton.click();
        Thread.sleep(3000);
    }

    @AfterMethod
    public void teardownTest() {
        driver.quit();
    }
}
