import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.openqa.selenium.JavascriptExecutor;


public class BornTodayTest {
    public WebDriver driver;
    public String testURL = "http://www.imdb.com";

    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
    }

    @Test
    public void BornTodayTest() throws InterruptedException {
        driver.manage().window().maximize();
        Thread.sleep(3000);

        WebElement menuButton = driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/label[1]"));
        menuButton.click();
        Thread.sleep(2000);

        WebElement bornTodayButton = driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/aside[1]/div/div[2]/div/div[4]/span/div/div/ul/a[1]"));
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", bornTodayButton);

        Thread.sleep(2000);

        WebElement mostRelevantButton = driver.findElement(By.xpath("/html/body/div[2]/main/div[2]/div[3]/section/section/div/section/section/div[2]/div/section/div[2]/div[2]/ul/li[1]/div[1]/div/div/div[1]/div[2]/div[2]/a"));
        mostRelevantButton.click();
        Thread.sleep(3000);
    }

    @AfterMethod
    public void teardownTest() {
        driver.quit();
    }
}
