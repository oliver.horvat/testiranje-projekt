import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class SignInTest {
    public WebDriver driver;
    public String testURL = "http://www.imdb.com";

    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
    }
    @Test
    public void SignInTest() throws InterruptedException {
        driver.manage().window().maximize();
        Thread.sleep(3000);
        WebElement signInButton= driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/div[5]/a/span"));
        signInButton.click();
        Thread.sleep(3000);

        WebElement ImdbLoginButton = driver.findElement(By.xpath("/html/body/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[1]/a[1]"));
        ImdbLoginButton.click();
        Thread.sleep(1500);

        WebElement usernameTextBox = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[2]/div/form/div/div/div/div[1]/input"));
        usernameTextBox.sendKeys("jokes1809@gmail.com");
        Thread.sleep(1500);

        WebElement passwordTextBox = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[2]/div/form/div/div/div/div[2]/input"));
        passwordTextBox.sendKeys("TestingProject1");
        Thread.sleep(1500);

        WebElement keepSingedInCheckbox = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[2]/div/form/div/div/div/div[3]/div/div/label/div/label/input"));
        keepSingedInCheckbox.click();
        Thread.sleep(1500);

        WebElement submitButton = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[2]/div/form/div/div/div/div[3]/span/span/input"));
        submitButton.click();
        Thread.sleep(3000);
     }

    @AfterMethod
    public void teardownTest() {
        driver.quit();
    }
}
