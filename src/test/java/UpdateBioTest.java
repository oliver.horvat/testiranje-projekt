import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import java.util.Random;

public class UpdateBioTest {
    public WebDriver driver;
    public String testURL = "http://www.imdb.com";

    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
    }
    @Test
    public void UpdateBioTest() throws InterruptedException {

        driver.manage().window().maximize();
        Thread.sleep(3000);
        WebElement signInButton= driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/div[5]/a/span"));
        signInButton.click();

        WebElement ImdbLoginButton = driver.findElement(By.xpath("/html/body/div[2]/div[1]/div[2]/div[1]/div[1]/div/div[1]/a[1]"));
        ImdbLoginButton.click();

        WebElement usernameTextBox = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[2]/div/form/div/div/div/div[1]/input"));
        usernameTextBox.sendKeys("jokes1809@gmail.com");

        WebElement passwordTextBox = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[2]/div/form/div/div/div/div[2]/input"));
        passwordTextBox.sendKeys("TestingProject1");

        WebElement submitButton = driver.findElement(By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[2]/div/form/div/div/div/div[3]/span/span/input"));
        submitButton.click();
        Thread.sleep(2000);

        WebElement profileButton = driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/div[5]"));
        profileButton.click();
        Thread.sleep(2000);

        WebElement settingsButton = driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/div[5]/div/div/div/div/span/ul/a[5]"));
        settingsButton.click();
        Thread.sleep(2000);

        WebElement editButton = driver.findElement(By.linkText("Edit profile"));
        editButton.click();
        Thread.sleep(2000);

        WebElement bioText = driver.findElement(By.cssSelector("#main > div > div:nth-child(2) > div:nth-child(2) > textarea"));
        bioText.clear();
        Random random = new Random();
        bioText.sendKeys(String.valueOf(random.nextInt()));
        Thread.sleep(1500);

        WebElement saveButton = driver.findElement(By.cssSelector("#main > div > div:nth-child(2) > div:nth-child(2) > div > div.auth-button-link.auth-button--primary"));
        saveButton.click();
        Thread.sleep(3000);

        WebElement homeButton = driver.findElement(By.xpath("/html/body/section[1]/nav/div[2]/a[1]"));
        homeButton.click();
        Thread.sleep(2000);

        profileButton = driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/div[5]"));
        profileButton.click();
        Thread.sleep(2000);

        WebElement activityButton = driver.findElement(By.linkText("Your activity"));
        activityButton.click();
        Thread.sleep(3000);
    }

    @AfterMethod
    public void teardownTest() {
        driver.quit();
    }
}
