import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.openqa.selenium.JavascriptExecutor;


public class WhatToWatchTest {
    public WebDriver driver;
    public String testURL = "http://www.imdb.com";

    @BeforeMethod
    public void setupTest() {
        System.setProperty("webdriver.chrome.driver", "./chromedriver.exe");
        driver = new ChromeDriver();
        driver.navigate().to(testURL);
    }

    @Test
    public void WhatToWatchTest() throws InterruptedException {
        driver.manage().window().maximize();
        Thread.sleep(3000);

        WebElement menuButton = driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/label[1]"));
        menuButton.click();
        Thread.sleep(2000);

        WebElement whatToWatchButton = driver.findElement(By.xpath("/html/body/div[2]/nav/div[2]/aside[1]/div/div[2]/div/div[2]/div[2]/span/div/div/ul/a[1]"));
        ((JavascriptExecutor)driver).executeScript("arguments[0].click();", whatToWatchButton);

        Thread.sleep(2000);

        WebElement mostPopularButton = driver.findElement(By.xpath("/html/body/div[2]/main/div[2]/section[2]/section/section/div[1]/ul/li[5]"));
        mostPopularButton.click();
        Thread.sleep(4000);

        WebElement mostPopularMovieButton = driver.findElement(By.xpath("/html/body/div[2]/main/div[2]/section[2]/section/section/div[2]/div/div[1]/div/div[1]/div[1]/a"));
        mostPopularMovieButton.click();
        Thread.sleep(3000);
    }

    @AfterMethod
    public void teardownTest() {
        driver.quit();
    }
}
